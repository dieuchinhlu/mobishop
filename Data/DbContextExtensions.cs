﻿using System;
using System.Collections.Generic;
using System.Linq;
using ECommerce.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Data
{
    public static class DbContextExtensions
    {
        public static RoleManager<AppRole> RoleManager { get; set; }
        public static UserManager<AppUser> UserManager { get; set; }

        public static void EnsureSeeded(this EcommerceContext context)
        {
            AddRoles(context);
            AddUsers(context);
            AddColoursFeaturesAndStorage(context);
            AddOperatingSystemsAndBrands(context);
            AddProducts(context);
        }

        private static void AddRoles(EcommerceContext context)
        {
            if (RoleManager.RoleExistsAsync("Admin").GetAwaiter().GetResult() == false)
            {
                RoleManager.CreateAsync(new AppRole("Admin")).GetAwaiter().GetResult();
            }

            if (RoleManager.RoleExistsAsync("Customer").GetAwaiter().GetResult() == false)
            {
                RoleManager.CreateAsync(new AppRole("Customer")).GetAwaiter().GetResult();
            }
        }

        private static void AddUsers(EcommerceContext context)
        {
            if (UserManager.FindByEmailAsync("admin@ludc.com").GetAwaiter().GetResult() == null)
            {
                var user = new AppUser
                {
                    FirstName = "Lu",
                    LastName = "Dieu Chinh",
                    UserName = "admin@ludc.com",
                    Email = "admin@ludc.com",
                    EmailConfirmed = true,
                    LockoutEnabled = false
                };

                UserManager.CreateAsync(user, "Passw0rd!").GetAwaiter().GetResult();
            }

            var admin = UserManager.FindByEmailAsync("admin@ludc.com").GetAwaiter().GetResult();

            if (UserManager.IsInRoleAsync(admin, "Admin").GetAwaiter().GetResult() == false)
            {
                UserManager.AddToRoleAsync(admin, "Admin");
            }
        }

        private static void AddColoursFeaturesAndStorage(EcommerceContext context)
        {
            if (context.Colours.Any() == false)
            {
                var colours = new List<string>() { "Black", "White", "Gold", "Silver", "Grey", "Spacegrey", "Red", "Pink", "Midnight black", "Ocean blue", "Lavender Purple" };

                colours.ForEach(c => context.Add(new Colour
                {
                    Name = c
                }));

                context.SaveChanges();
            }

            if (context.Features.Any() == false)
            {
                var features = new List<string>() { "3G", "4G", "5G", "Bluetooth", "WiFi", "Fast charge", "GPS", "NFC" };

                features.ForEach(f => context.Add(new Feature
                {
                    Name = f
                }));

                context.SaveChanges();
            }

            if (context.Storage.Any() == false)
            {
                var storage = new List<int>() { 4, 8, 16, 32, 64, 128, 256, 512 };

                storage.ForEach(s => context.Storage.Add(new Storage
                {
                    Capacity = s
                }));

                context.SaveChanges();
            }
        }

        private static void AddOperatingSystemsAndBrands(EcommerceContext context)
        {
            if (context.OS.Any() == false)
            {
                var os = new List<string>() { "Android", "iOS", "Windows Phone" };

                os.ForEach(o => context.OS.Add(new OS
                {
                    Name = o
                }));

                context.SaveChanges();
            }

            if (context.Brands.Any() == false)
            {
                var brands = new List<string>() { "Apple", "Samsung", "Sony", "Oppo", "Huawei", "Xiaomi", "Nokia" };

                brands.ForEach(b => context.Brands.Add(new Brand
                {
                    Name = b
                }));

                context.SaveChanges();
            }
        }

        private static void AddProducts(EcommerceContext context)
        {
            if (context.Products.Any() == false)
            {
                var products = new List<Product>()
                    {
                      new Product
                      {
                        Name = "Apple iPhone Xs",
                        Slug = "apple-iphone-xs",
                        Thumbnail = "/assets/images/iphone1.jpg",
                        ShortDescription = "Hoàn toàn xứng đáng với những gì được mong chờ, phiên bản cao cấp nhất iPhone Xs Max của Apple năm nay nổi bật với chip A12 Bionic mạnh mẽ, màn hình rộng đến 6.5 inch, cùng camera kép trí tuệ nhân tạo và Face ID được nâng cấp.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 5.8M,
                        TalkTime = 8M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Apple"),
                        OS = context.OS.Single(os => os.Name == "iOS"),
                        Images = new List<Image>
                        {
                          new Image { Url = "/assets/images/iphone1.jpg" },
                          new Image { Url = "/assets/images/iphone2.jpg" },
                          new Image { Url = "/assets/images/iphone3.jpg" },
                          new Image { Url = "/assets/images/iphone4.jpg" },
                          new Image { Url = "/assets/images/iphone5.jpg" },
                          new Image { Url = "/assets/images/iphone6.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Spacegrey"),
                            Storage = context.Storage.Single(s => s.Capacity == 64),
                            Price = 999M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Spacegrey"),
                            Storage = context.Storage.Single(s => s.Capacity == 256),
                            Price = 1149M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Spacegrey"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1349M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Gold"),
                                Storage = context.Storage.Single(s => s.Capacity == 64),
                                Price = 999M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Gold"),
                                Storage = context.Storage.Single(s => s.Capacity == 256),
                                Price = 1149M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Gold"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1349M
                            },new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Silver"),
                                Storage = context.Storage.Single(s => s.Capacity == 64),
                                Price = 999M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Silver"),
                                Storage = context.Storage.Single(s => s.Capacity == 256),
                                Price = 1149M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Silver"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1349M
                            }
                        }
                      },
                      new Product
                      {
                        Name = "Apple iPhone Xs Max",
                        Slug = "apple-iphone-xs-max",
                        Thumbnail = "/assets/images/iphone1.jpg",
                        ShortDescription = "Hoàn toàn xứng đáng với những gì được mong chờ, phiên bản cao cấp nhất iPhone Xs Max của Apple năm nay nổi bật với chip A12 Bionic mạnh mẽ, màn hình rộng đến 6.5 inch, cùng camera kép trí tuệ nhân tạo và Face ID được nâng cấp.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 6.5M,
                        TalkTime = 9M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Apple"),
                        OS = context.OS.Single(os => os.Name == "iOS"),
                        Images = new List<Image>
                        {
                          new Image { Url = "/assets/images/iphone1.jpg" },
                          new Image { Url = "/assets/images/iphone2.jpg" },
                          new Image { Url = "/assets/images/iphone3.jpg" },
                          new Image { Url = "/assets/images/iphone4.jpg" },
                          new Image { Url = "/assets/images/iphone5.jpg" },
                          new Image { Url = "/assets/images/iphone6.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Spacegrey"),
                            Storage = context.Storage.Single(s => s.Capacity == 64),
                            Price = 1099M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Spacegrey"),
                            Storage = context.Storage.Single(s => s.Capacity == 256),
                            Price = 1299M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Spacegrey"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1499M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Gold"),
                                Storage = context.Storage.Single(s => s.Capacity == 64),
                                Price = 1099M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Gold"),
                                Storage = context.Storage.Single(s => s.Capacity == 256),
                                Price = 1299M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Gold"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1499M
                            },new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Silver"),
                                Storage = context.Storage.Single(s => s.Capacity == 64),
                                Price = 1099M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Silver"),
                                Storage = context.Storage.Single(s => s.Capacity == 256),
                                Price = 1299M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Silver"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1499M
                            }
                        }
                      },
                      new Product
                      {
                        Name = "Samsung Galaxy Note 9",
                        Slug = "samsung-galaxy-note9",
                        Thumbnail = "/assets/images/samsung1.jpg",
                        ShortDescription = "Mang lại sự cải tiến đặc biệt trong cây bút S-Pen, siêu phẩm Samsung Galaxy Note 9 còn sở hữu dung lượng pin khủng lên tới 4.000 mAh cùng hiệu năng mạnh mẽ vượt bậc, xứng đáng là một trong những chiếc điện thoại cao cấp nhất của Samsung.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 5.8M,
                        TalkTime = 8M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Samsung"),
                        OS = context.OS.Single(os => os.Name == "Android"),
                        Images = new List<Image>
                        {
                          new Image { Url = "/assets/images/samsung1.jpg" },
                          new Image { Url = "/assets/images/samsung2.jpg" },
                          new Image { Url = "/assets/images/samsung3.jpg" },
                          new Image { Url = "/assets/images/samsung4.jpg" },
                          new Image { Url = "/assets/images/samsung5.jpg" },
                          new Image { Url = "/assets/images/samsung6.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Midnight black"),
                            Storage = context.Storage.Single(s => s.Capacity == 128),
                            Price = 899M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Midnight black"),
                            Storage = context.Storage.Single(s => s.Capacity == 512),
                            Price = 1099M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Ocean blue"),
                                Storage = context.Storage.Single(s => s.Capacity == 128),
                                Price = 899M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Ocean blue"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1099M
                            },new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Lavender Purple"),
                                Storage = context.Storage.Single(s => s.Capacity == 128),
                                Price = 899M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Lavender Purple"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1099M
                            }
                        }
                      },
                      new Product
                      {
                        Name = "Samsung Galaxy S9",
                        Slug = "samsung-galaxy-s9",
                        Thumbnail = "/assets/images/samsungs91.jpg",
                        ShortDescription = "Mang lại sự cải tiến đặc biệt trong cây bút S-Pen, siêu phẩm Samsung Galaxy Note 9 còn sở hữu dung lượng pin khủng lên tới 4.000 mAh cùng hiệu năng mạnh mẽ vượt bậc, xứng đáng là một trong những chiếc điện thoại cao cấp nhất của Samsung.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 5.8M,
                        TalkTime = 8M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Samsung"),
                        OS = context.OS.Single(os => os.Name == "Android"),
                        Images = new List<Image>
                        {
                          new Image { Url = "/assets/images/samsungs91.jpg" },
                          new Image { Url = "/assets/images/samsungs92.jpg" },
                          new Image { Url = "/assets/images/samsungs93.jpg" },
                          new Image { Url = "/assets/images/samsungs94.jpg" },
                          new Image { Url = "/assets/images/samsungs95.jpg" },
                          new Image { Url = "/assets/images/samsungs96.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Midnight black"),
                            Storage = context.Storage.Single(s => s.Capacity == 128),
                            Price = 869M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Midnight black"),
                            Storage = context.Storage.Single(s => s.Capacity == 512),
                            Price = 999M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Ocean blue"),
                                Storage = context.Storage.Single(s => s.Capacity == 128),
                                Price = 869M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Ocean blue"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 999M
                            },new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Lavender Purple"),
                                Storage = context.Storage.Single(s => s.Capacity == 128),
                                Price = 869M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Lavender Purple"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 999M
                            }
                        }
                      },
                      new Product
                      {
                        Name = "Samsung Galaxy S9+",
                        Slug = "samsung-galaxy-s9-plus",
                        Thumbnail = "/assets/images/samsungs911.jpg",
                        ShortDescription = "Mang lại sự cải tiến đặc biệt trong cây bút S-Pen, siêu phẩm Samsung Galaxy Note 9 còn sở hữu dung lượng pin khủng lên tới 4.000 mAh cùng hiệu năng mạnh mẽ vượt bậc, xứng đáng là một trong những chiếc điện thoại cao cấp nhất của Samsung.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 5.8M,
                        TalkTime = 8M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Samsung"),
                        OS = context.OS.Single(os => os.Name == "Android"),
                        Images = new List<Image>
                        {
                            new Image { Url = "/assets/images/samsungs91.jpg" },
                            new Image { Url = "/assets/images/samsungs92.jpg" },
                            new Image { Url = "/assets/images/samsungs93.jpg" },
                            new Image { Url = "/assets/images/samsungs94.jpg" },
                            new Image { Url = "/assets/images/samsungs95.jpg" },
                            new Image { Url = "/assets/images/samsungs96.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Midnight black"),
                            Storage = context.Storage.Single(s => s.Capacity == 128),
                            Price = 969M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Midnight black"),
                            Storage = context.Storage.Single(s => s.Capacity == 512),
                            Price = 1099M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Ocean blue"),
                                Storage = context.Storage.Single(s => s.Capacity == 128),
                                Price = 969M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Ocean blue"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1099M
                            },new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Lavender Purple"),
                                Storage = context.Storage.Single(s => s.Capacity == 128),
                                Price = 969M
                            },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "Lavender Purple"),
                                Storage = context.Storage.Single(s => s.Capacity == 512),
                                Price = 1099M
                            }
                        }
                      },
                      new Product
                      {
                        Name = "Nokia 3 UK-SIM Free Smartphone",
                        Slug = "nokia-3-free-sim",
                        Thumbnail = "/assets/images/nokia1.jpg",
                        ShortDescription = "Mang lại sự cải tiến đặc biệt trong cây bút S-Pen, siêu phẩm Samsung Galaxy Note 9 còn sở hữu dung lượng pin khủng lên tới 4.000 mAh cùng hiệu năng mạnh mẽ vượt bậc, xứng đáng là một trong những chiếc điện thoại cao cấp nhất của Samsung.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 5M,
                        TalkTime = 8M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Nokia"),
                        OS = context.OS.Single(os => os.Name == "Android"),
                        Images = new List<Image>
                        {
                          new Image { Url = "/assets/images/nokia1.jpg" },
                          new Image { Url = "/assets/images/nokia2.jpg" },
                          new Image { Url = "/assets/images/nokia3.jpg" },
                          new Image { Url = "/assets/images/nokia4.jpg" },
                          new Image { Url = "/assets/images/nokia5.jpg" },
                          new Image { Url = "/assets/images/nokia6.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Black"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 104.47M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Blue"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 104.99M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "White"),
                                Storage = context.Storage.Single(s => s.Capacity == 16),
                                Price = 79M
                            }
                        }
                      },
                      new Product
                      {
                        Name = "Nokia 3.1 UK-SIM Free Smartphone",
                        Slug = "nokia-31-free-sim",
                        Thumbnail = "/assets/images/nokia1.jpg",
                        ShortDescription = "Mang lại sự cải tiến đặc biệt trong cây bút S-Pen, siêu phẩm Samsung Galaxy Note 9 còn sở hữu dung lượng pin khủng lên tới 4.000 mAh cùng hiệu năng mạnh mẽ vượt bậc, xứng đáng là một trong những chiếc điện thoại cao cấp nhất của Samsung.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 5M,
                        TalkTime = 8M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Nokia"),
                        OS = context.OS.Single(os => os.Name == "Android"),
                        Images = new List<Image>
                        {
                          new Image { Url = "/assets/images/nokia1.jpg" },
                          new Image { Url = "/assets/images/nokia2.jpg" },
                          new Image { Url = "/assets/images/nokia3.jpg" },
                          new Image { Url = "/assets/images/nokia4.jpg" },
                          new Image { Url = "/assets/images/nokia5.jpg" },
                          new Image { Url = "/assets/images/nokia6.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Black"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 137.47M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Blue"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 117.99M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "White"),
                                Storage = context.Storage.Single(s => s.Capacity == 16),
                                Price = 79M
                            }
                        }
                      },
                      new Product
                      {
                        Name = "Nokia 4.1 UK-SIM Free Smartphone",
                        Slug = "nokia-41-free-sim",
                        Thumbnail = "/assets/images/nokia2.jpg",
                        ShortDescription = "Mang lại sự cải tiến đặc biệt trong cây bút S-Pen, siêu phẩm Samsung Galaxy Note 9 còn sở hữu dung lượng pin khủng lên tới 4.000 mAh cùng hiệu năng mạnh mẽ vượt bậc, xứng đáng là một trong những chiếc điện thoại cao cấp nhất của Samsung.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 5M,
                        TalkTime = 8M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Nokia"),
                        OS = context.OS.Single(os => os.Name == "Android"),
                        Images = new List<Image>
                        {
                          new Image { Url = "/assets/images/nokia1.jpg" },
                          new Image { Url = "/assets/images/nokia2.jpg" },
                          new Image { Url = "/assets/images/nokia3.jpg" },
                          new Image { Url = "/assets/images/nokia4.jpg" },
                          new Image { Url = "/assets/images/nokia5.jpg" },
                          new Image { Url = "/assets/images/nokia6.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Black"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 147.47M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Blue"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 127.99M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "White"),
                                Storage = context.Storage.Single(s => s.Capacity == 16),
                                Price = 89M
                            }
                        }
                      },
                      new Product
                      {
                        Name = "Nokia 5.1 UK-SIM Free Smartphone",
                        Slug = "nokia-51-free-sim",
                        Thumbnail = "/assets/images/nokia3.jpg",
                        ShortDescription = "Mang lại sự cải tiến đặc biệt trong cây bút S-Pen, siêu phẩm Samsung Galaxy Note 9 còn sở hữu dung lượng pin khủng lên tới 4.000 mAh cùng hiệu năng mạnh mẽ vượt bậc, xứng đáng là một trong những chiếc điện thoại cao cấp nhất của Samsung.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 5M,
                        TalkTime = 8M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Nokia"),
                        OS = context.OS.Single(os => os.Name == "Android"),
                        Images = new List<Image>
                        {
                          new Image { Url = "/assets/images/nokia1.jpg" },
                          new Image { Url = "/assets/images/nokia2.jpg" },
                          new Image { Url = "/assets/images/nokia3.jpg" },
                          new Image { Url = "/assets/images/nokia4.jpg" },
                          new Image { Url = "/assets/images/nokia5.jpg" },
                          new Image { Url = "/assets/images/nokia6.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Black"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 157.47M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Blue"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 137.99M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "White"),
                                Storage = context.Storage.Single(s => s.Capacity == 16),
                                Price = 99M
                            }
                        }
                      },
                      new Product
                      {
                        Name = "Sony Xperia XZ",
                        Slug = "sony-xperia-xz",
                        Thumbnail = "/assets/images/sony1.jpg",
                        ShortDescription = "Mang lại sự cải tiến đặc biệt trong cây bút S-Pen, siêu phẩm Samsung Galaxy Note 9 còn sở hữu dung lượng pin khủng lên tới 4.000 mAh cùng hiệu năng mạnh mẽ vượt bậc, xứng đáng là một trong những chiếc điện thoại cao cấp nhất của Samsung.",
                        Description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis tempora ad cum laudantium, omnis fugit amet iure animi corporis labore repellat magnam perspiciatis explicabo maiores fuga provident a obcaecati tenetur nostrum, quidem quod dignissimos, voluptatem quasi? Nisi quaerat, fugit voluptas ducimus facilis impedit quod dicta, laborum sint iure nihil veniam aspernatur delectus officia culpa, at cupiditate? Totam minima ut deleniti laboriosam dolores cumque in, nesciunt optio! Quod recusandae voluptate facere pariatur soluta vel corrupti tenetur aut maiores, cumque mollitia fugiat laudantium error odit voluptas nobis laboriosam quo, rem deleniti? Iste quidem amet perferendis sed iusto tempora modi illo tempore quibusdam laborum? Dicta aliquam libero, facere, maxime corporis qui officiis explicabo aspernatur non consequatur mollitia iure magnam odit enim. Eligendi suscipit, optio officiis repellat eos quis iure? Omnis, error aliquid quibusdam iste amet nihil nisi cumque magni sequi enim illo autem nesciunt optio accusantium animi commodi tenetur neque eum vitae est.",
                        ScreenSize = 5M,
                        TalkTime = 8M,
                        StandbyTime = 36M,
                        Brand = context.Brands.Single(b => b.Name == "Sony"),
                        OS = context.OS.Single(os => os.Name == "Android"),
                        Images = new List<Image>
                        {
                          new Image { Url = "/assets/images/sony1.jpg" },
                          new Image { Url = "/assets/images/sony2.jpg" },
                          new Image { Url = "/assets/images/sony3.jpg" },
                          new Image { Url = "/assets/images/sony4.jpg" },
                          new Image { Url = "/assets/images/sony5.jpg" },
                          new Image { Url = "/assets/images/sony6.jpg" }
                        },
                        ProductFeatures = new List<ProductFeature>
                        {
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "3G")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "Bluetooth")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "WiFi")
                          },
                          new ProductFeature
                          {
                            Feature = context.Features.Single(f => f.Name == "GPS")
                          }
                        },
                        ProductVariants = new List<ProductVariant>
                        {
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Black"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 157.47M
                          },
                          new ProductVariant
                          {
                            Colour = context.Colours.Single(c => c.Name == "Blue"),
                            Storage = context.Storage.Single(s => s.Capacity == 16),
                            Price = 137.99M
                          },
                            new ProductVariant
                            {
                                Colour = context.Colours.Single(c => c.Name == "White"),
                                Storage = context.Storage.Single(s => s.Capacity == 16),
                                Price = 99M
                            }
                        }
                      },
                    };

                context.Products.AddRange(products);
                context.SaveChanges();
            }
        }
    }
}